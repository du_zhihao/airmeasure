/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-11-21     admin       the first version
 */
#ifndef APPLICATIONS_MY_UART2_H_
#define APPLICATIONS_MY_UART2_H_


//定义数组长度
#define GPS_Buffer_Length 80
#define UTCTime_Length 11
#define latitude_Length 11
#define N_S_Length 2
#define longitude_Length 12
#define E_W_Length 2

typedef struct SaveData
{
    char GPS_Buffer[GPS_Buffer_Length];
    char isGetData;     //是否获取到GPS数据
    char isParseData;   //是否完成解析
    char UTCTime[UTCTime_Length];       //UTC时间
    char latitude[latitude_Length];     //维度
    char N_S[N_S_Length];       //NS
    char longitude[longitude_Length];       //经度
    char E_W[E_W_Length];       //EW
    char isUsefull;     //定位信息是否有效
    char text;
} saveData;
extern saveData Save_Data;

int uart2_sample();

#endif /* APPLICATIONS_MY_UART2_H_ */
