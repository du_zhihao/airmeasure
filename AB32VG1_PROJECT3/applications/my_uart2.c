/*
* 程序清单：这是一个 串口 设备使用例程
* 例程导出了 uart_sample 命令到控制终端
* 命令调用格式： uart_sample uart1
* 命令解释：命令第二个参数是要使用的串口设备名称，为空则使用默认的串口设备
* 程序功能：通过串口输出字符串 "hello RT-Thread!"，然后错位输出输入的字符
*/
#include <rtthread.h>
#include "string.h"
//#include "my_gps.h"
#include "my_uart2.h"

saveData Save_Data; //!!!

#define SAMPLE_UART_NAME "uart2"
unsigned int point1;     //接收到帧头标志位后清零
#define USART2_REC_LEN           100     //最大接收缓存 200
#define false 0
#define true 1
char assure_gps = 0;

char USART2_RX_BUF[USART2_REC_LEN]; //接收缓存

void CLR_Buf(void)                           //串口缓存清理
{
    memset(USART2_RX_BUF, 0, USART2_REC_LEN);      //清空
  point1 = 0;
}

void clrStruct()
{
    rt_kprintf("clrStruct...\n");
    Save_Data.isGetData = false;
    Save_Data.isParseData = false;
    Save_Data.isUsefull = false;
    memset(Save_Data.GPS_Buffer, 0, GPS_Buffer_Length);      //清空
    memset(Save_Data.UTCTime, 0, UTCTime_Length);
    memset(Save_Data.latitude, 0, latitude_Length);
    memset(Save_Data.N_S, 0, N_S_Length);
    memset(Save_Data.longitude, 0, longitude_Length);
    memset(Save_Data.E_W, 0, E_W_Length);

}

void errorLog(int num)
{
    while(1)
    {
        rt_kprintf("ERROR%d",num);
    }
}

void parseGpsBuffer()
{
    char *subString;
    char *subStringNext;
    char i = 0;
    if (Save_Data.isGetData)
    {

        Save_Data.isGetData = false;
        //rt_kprintf("**************\r\n");
        //rt_kprintf(Save_Data.GPS_Buffer);
       // rt_kprintf("\n");
        //rt_kprintf("%d",Save_Data.text);


        for (i = 0 ; i <= 5 ; i++)
        {
            if (i == 0)
            {
                if ((subString = strstr(Save_Data.GPS_Buffer, ",")) == NULL)    //第一次出现","的地址
                    errorLog(1);    //解析错误
            }
            else
            {
                subString++;
                if ((subStringNext = strstr(subString, ",")) != NULL)
                {

                    char usefullBuffer[2];
                    switch(i)
                    {
                        case 1:memcpy(Save_Data.UTCTime, subString, subStringNext - subString);break;   //UTC
                        case 2:memcpy(Save_Data.latitude, subString, subStringNext - subString);break;  //纬度
                        case 3:memcpy(Save_Data.N_S, subString, subStringNext - subString);break;   //纬度N/S
                        case 4:memcpy(Save_Data.longitude, subString, subStringNext - subString);break; //经度
                        case 5:memcpy(Save_Data.E_W, subString, subStringNext - subString);break;   //经度E/W

                        default:break;
                    }

                    subString = subStringNext;
                    Save_Data.isParseData = true;
                        Save_Data.isUsefull = true;
                }
                else
                {
                    errorLog(2);    //解析错误
                }
            }


        }
    }
}

void printGpsBuffer()
{
    if (Save_Data.isParseData)
    {
        Save_Data.isParseData = false;

        rt_kprintf("Save_Data.UTCTime = ");
        rt_kprintf(Save_Data.UTCTime);
        rt_kprintf("\r\n");

        if(Save_Data.isUsefull)
        {
            Save_Data.isUsefull = false;
            rt_kprintf("Save_Data.latitude = ");
            rt_kprintf(Save_Data.latitude);
            rt_kprintf("\r\n");


            rt_kprintf("Save_Data.N_S = ");
            rt_kprintf(Save_Data.N_S);
            rt_kprintf("\r\n");

            rt_kprintf("Save_Data.longitude = ");
            rt_kprintf(Save_Data.longitude);
            rt_kprintf("\r\n");

            rt_kprintf("Save_Data.E_W = ");
            rt_kprintf(Save_Data.E_W);
            rt_kprintf("\r\n");
        }
        else
        {
            rt_kprintf("GPS DATA is not usefull!\r\n");
        }
    }
}

/* 用于接收消息的信号量 */
static struct rt_semaphore rx_sem;
static rt_device_t serial;
/* 接收数据回调函数 */
static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
/* 串口接收到数据后产生中断，调用此回调函数，然后发送接收信号量 */
rt_sem_release(&rx_sem);
return RT_EOK;
}
static void serial_thread_entry(void *parameter)
{
    char ch;        //串口2每次接收的1个字节
    Save_Data.text = 123;
    while (1)
    {
        /* 从串口读取一个字节的数据，没有读取到则等待接收信号量 */
        while (rt_device_read(serial, -1, &ch, 1) != 1)
            {
            /* 阻塞等待接收信号量，等到信号量后再次读取数据 */
            rt_sem_take(&rx_sem, RT_WAITING_FOREVER);
            }
        /* 读取到的数据通过串口错位输出 */

        if(ch == '$')
        {
            point1 = 0;
        }
        USART2_RX_BUF[point1++] = ch;
        if(USART2_RX_BUF[0] == '$' && USART2_RX_BUF[4] == 'G' && USART2_RX_BUF[5] == 'A' )
        {
            if(ch == '\n')
            {
                memset(Save_Data.GPS_Buffer, 0, GPS_Buffer_Length);      //清空
                memcpy(Save_Data.GPS_Buffer, USART2_RX_BUF, point1);     //保存数据
                Save_Data.isGetData = true;
                point1 = 0;
                memset(USART2_RX_BUF, 0, USART2_REC_LEN);      //清空
            }
        }
        if(point1 >= USART2_REC_LEN)
        {
            point1 = USART2_REC_LEN;
        }

           if( Save_Data.isGetData == true)
           {
               Save_Data.text = 234;
               parseGpsBuffer();
               //printGpsBuffer();

               //rt_thread_mdelay(3000);

           }
    }
}








int uart2_sample()
{

    rt_err_t ret = RT_EOK;
    char uart_name[RT_NAME_MAX];
    char str[] = "hello RT-Thread!\r\n";
    rt_strncpy(uart_name, SAMPLE_UART_NAME, RT_NAME_MAX);

    /* 查找系统中的串口设备 */
    serial = rt_device_find(uart_name);
    if (!serial)
    {
        rt_kprintf("find %s failed!\n", uart_name);
    return RT_ERROR;
    }
    /* 初始化信号量 */
    rt_sem_init(&rx_sem, "rx_sem", 0, RT_IPC_FLAG_FIFO);
    /* 以中断接收及轮询发送模式打开串口设备 */
    rt_device_open(serial, RT_DEVICE_FLAG_INT_RX);
    /* 设置接收回调函数 */
    rt_device_set_rx_indicate(serial, uart_input);
    /* 发送字符串 */
    rt_device_write(serial, 0, str, (sizeof(str) - 1));
    /* 创建 serial 线程 */
    rt_thread_t thread = rt_thread_create("uart2", serial_thread_entry, RT_NULL, 1024, 15, 10);

    /* 创建成功则启动线程 */
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
        rt_kprintf("uart2_thread creat successful...\n");
    }
    else
    {
        ret = RT_ERROR;
    }
    return ret;
}
/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(uart2_sample, uart device sample);
//INIT_APP_EXPORT(uart2_sample);
