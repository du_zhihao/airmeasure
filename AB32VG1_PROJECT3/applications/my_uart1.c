/*
* 程序清单：这是一个 串口 设备使用例程
* 例程导出了 uart_sample 命令到控制终端
* 命令调用格式： uart_sample uart1
* 命令解释：命令第二个参数是要使用的串口设备名称，为空则使用默认的串口设备
* 程序功能：通过串口输出字符串 "hello RT-Thread!"，然后错位输出输入的字符
*/
#include <rtthread.h>
#include "my_uart2.h"
#include"string.h"
#include"stdlib.h"
#include"stdio.h"
#include"dht11_sample.h"
#include <ctype.h>
 #include"cJSON.h"
//#include"dht11_sample.c"

//saveData Save_Data; //!!!

#define SAMPLE_UART_NAME "uart1"
/* 用于接收消息的信号量 */
static struct rt_semaphore rx_sem;
static rt_device_t serial;
/* 接收数据回调函数 */
static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
    /* 串口接收到数据后产生中断，调用此回调函数，然后发送接收信号量 */
    rt_sem_release(&rx_sem);
    return RT_EOK;
}

//字符串转十六进制
char tem_str[2]={0};
char tem_hex[2] = {0};
void StringToHex(char *str, unsigned char *strhex)
{
    uint8_t i,cnt=0;
    char *p = str;             //直针p初始化为指向str
    uint8_t len = strlen(str); //获取字符串中的字符个数

    while(*p != '\0') {        //结束符判断
        for (i = 0; i < len; i ++)  //循环判断当前字符是数字还是小写字符还是大写字母
        {
            if ((*p >= '0') && (*p <= '9')) //当前字符为数字0~9时
                strhex[cnt] = *p - '0' + 0x30;//转为十六进制

            if ((*p >= 'A') && (*p <= 'Z')) //当前字符为大写字母A~Z时
                strhex[cnt] = *p - 'A' + 0x41;//转为十六进制

            if ((*p >= 'a') && (*p <= 'z')) //当前字符为小写字母a~z时
                strhex[cnt] = *p - 'a' + 0x61;  //转为十六进制

            p ++;    //指向下一个字符
            cnt ++;
        }
    }
}

/**function: CharToHex()
*** ACSII change to 16 hex
*** input:ACSII
***Return :Hex
**/
unsigned char CharToHex(unsigned char bHex)
{
    if((bHex>=0)&&(bHex<=9))
    {
        bHex += 0x30;
    }
    else if((bHex>=10)&&(bHex<=15))//Capital
    {
        bHex += 0x37;
    }
    else
    {
        bHex = 0xff;
    }
    return bHex;
}



static void serial_thread_entry(void *parameter)
{
        char i;
        char temp[13]={0x03,0x00,0x0a,0x7b,0x22,0x74,0x65,0x6d,0x22,0x3a,0x32,0x32,0x7d};
        char humi[13]={0x03,0x00,0x0a,0x7b,0x22,0x68,0x75,0x6d,0x22,0x3a,0x32,0x32,0x7d};
   /*char location[98]={ 0x01,0x00,0x5f,0x7b,0x22,0x64,0x61,0x74,0x61,0x73,
                       0x74,0x72,0x65,0x61,0x6d,0x73,0x22,0x3a,0x5b,0x7b,
                       0x22,0x69,0x64,0x22,0x3a,0x22,0x6c,0x6f,0x63,0x61,
                       0x74,0x69,0x6f,0x6e,0x22,0x2c,0x22,0x64,0x61,0x74,
                       0x61,0x70,0x6f,0x69,0x6e,0x74,0x73,0x22,0x3a,0x5b,
                       0x7b,0x22,0x76,0x61,0x6c,0x75,0x65,0x22,0x3a,0x7b,
                       0x22,0x6c,0x6f,0x6e,0x22,0x3a,0x31,0x31,0x31,0x2e,
                       0x39,0x32,0x32,0x35,0x36,0x31,0x2c,0x22,0x6c,0x61,
                       0x74,0x22,0x3a,0x31,0x31,0x2e,0x36,0x39,0x34,0x39,
                       0x34,0x30,0x7d,0x7d,0x5d,0x7d,0x5d,0x7d};*/
        char location[98] = { 0x01,0x00,0x5f,0x7b,0x22,0x64,0x61,0x74,0x61,0x73,
                             0x74,0x72,0x65,0x61,0x6d,0x73,0x22,0x3a,0x5b,0x7b,
                             0x22,0x69,0x64,0x22,0x3a,0x22,0x6c,0x6f,0x63,0x61,
                             0x74,0x69,0x6f,0x6e,0x22,0x2c,0x22,0x64,0x61,0x74,
                             0x61,0x70,0x6f,0x69,0x6e,0x74,0x73,0x22,0x3a,0x5b,
                             0x7b,0x22,0x76,0x61,0x6c,0x75,0x65,0x22,0x3a,0x7b,
                             0x22,0x6c,0x6f,0x6e,0x22,0x3a,0x31,0x31,0x33,0x2e,
                             0x39,0x32,0x32,0x35,0x36,0x31,0x2c,0x22,0x6c,0x61,
                             0x74,0x22,0x3a,0x32,0x32,0x2e,0x36,0x39,0x34,0x39,
                             0x34,0x30,0x7d,0x7d,0x5d,0x7d,0x5d,0x7d };
    //rt_device_write(serial , 0 , temp ,13 ); //串口发送
    while(1)
    {
        temp[10]=CharToHex(temperature/10);
        temp[11]=CharToHex(temperature%10);
        rt_device_write(serial , 0 , temp ,13 ); //串口发送
        //rt_thread_mdelay(1000);

        humi[10]=CharToHex(humidity/10);
        humi[11]=CharToHex(humidity%10);
        rt_device_write(serial , 0 , humi ,13 ); //串口发送
        //rt_thread_mdelay(1000);


        //for(i=0;i<11;i++)
/*
        location[66] = Save_Data.longitude[0];
        location[67] = Save_Data.longitude[1];
        location[68] = Save_Data.longitude[2];
*/


/*
        location[70] = Save_Data.longitude[2];
        location[71] = Save_Data.longitude[2];
        location[72] = Save_Data.longitude[2];
        location[73] = Save_Data.longitude[2];
        location[74] = Save_Data.longitude[2];
        location[75] = Save_Data.longitude[2];
*/

/*
        location[83] = Save_Data.latitude[0];
        location[84] = Save_Data.latitude[1];
*/



/*
        location[86] =  Save_Data.latitude[1];
        location[87] =  Save_Data.latitude[1];
        location[88] =  Save_Data.latitude[1];
        location[89] =  Save_Data.latitude[1];
        location[90] =  Save_Data.latitude[1];
        location[91] =  Save_Data.latitude[1];
*/

        rt_device_write(serial , 0 , location ,98 ); //串口发送
        rt_thread_mdelay(1000);
        //rt_kprintf("lat: %s",Save_Data.latitude);

        //字符串拼接
        //char *gps_date = (char*)malloc(strlen(Save_Data.UTCTime) + strlen(temperature) + strlen(humidity) + 15);
        //sprintf(gps_date,"UTC=%s;tem=%d;hum=%d;",Save_Data.UTCTime,temperature,humidity);
        //rt_device_write(serial , 0 , gps_date , rt_strlen(gps_date) ); //串口发送
        //rt_device_write(serial , 0 , out1 , 5 ); //串口发送



    }
}
 int uart1_sample()
{
    rt_err_t ret = RT_EOK;
    char uart_name[RT_NAME_MAX];
    char str[] = "hello RT-Thread!\r\n";



    rt_strncpy(uart_name, SAMPLE_UART_NAME, RT_NAME_MAX);
    /* 查找系统中的串口设备 */
    serial = rt_device_find(uart_name);
    if (!serial)
    {
        rt_kprintf("find %s failed!\n", uart_name);
        return RT_ERROR;
    }
    /* 初始化信号量 */
    rt_sem_init(&rx_sem, "rx_sem", 0, RT_IPC_FLAG_FIFO);
    /* 以中断接收及轮询发送模式打开串口设备 */
    rt_device_open(serial, RT_DEVICE_FLAG_INT_RX);
    /* 设置接收回调函数 */
    rt_device_set_rx_indicate(serial, uart_input);
    /* 发送字符串 */
    rt_device_write(serial, 0, str, (sizeof(str) - 1));
    /* 创建 serial 线程 */
    rt_thread_t thread = rt_thread_create("uart1", serial_thread_entry, RT_NULL, 1024, 25, 10);
    /* 创建成功则启动线程 */
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
        rt_kprintf("uart1_thread creat successful...\n");
    }
    else
    {
        ret = RT_ERROR;
    }
    return ret;
}
/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(uart1_sample, uart device sample);
//INIT_APP_EXPORT(uart1_sample);
