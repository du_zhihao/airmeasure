/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-11-21     admin       the first version
 */
#ifndef APPLICATIONS_MY_UART1_H_
#define APPLICATIONS_MY_UART1_H_

 int uart1_sample();

#endif /* APPLICATIONS_MY_UART1_H_ */
