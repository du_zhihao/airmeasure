################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../rt-thread/components/drivers/src/completion.c \
../rt-thread/components/drivers/src/dataqueue.c \
../rt-thread/components/drivers/src/pipe.c \
../rt-thread/components/drivers/src/ringblk_buf.c \
../rt-thread/components/drivers/src/ringbuffer.c \
../rt-thread/components/drivers/src/waitqueue.c \
../rt-thread/components/drivers/src/workqueue.c 

OBJS += \
./rt-thread/components/drivers/src/completion.o \
./rt-thread/components/drivers/src/dataqueue.o \
./rt-thread/components/drivers/src/pipe.o \
./rt-thread/components/drivers/src/ringblk_buf.o \
./rt-thread/components/drivers/src/ringbuffer.o \
./rt-thread/components/drivers/src/waitqueue.o \
./rt-thread/components/drivers/src/workqueue.o 

C_DEPS += \
./rt-thread/components/drivers/src/completion.d \
./rt-thread/components/drivers/src/dataqueue.d \
./rt-thread/components/drivers/src/pipe.d \
./rt-thread/components/drivers/src/ringblk_buf.d \
./rt-thread/components/drivers/src/ringbuffer.d \
./rt-thread/components/drivers/src/waitqueue.d \
./rt-thread/components/drivers/src/workqueue.d 


# Each subdirectory must supply rules for building sources it contributes
rt-thread/components/drivers/src/%.o: ../rt-thread/components/drivers/src/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\include\libc" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\applications" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\board" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libcpu\cpu" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_drivers\config" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_drivers" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\ab32vg1_hal\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\ab32vg1_hal" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\bmsis\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\bmsis" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\bluetrum_sdk-latest" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\cJSON-v1.7.15" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\dht11-latest" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\drivers\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\drivers\sensors" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\finsh" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\libc\compilers\common" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\libc\compilers\newlib" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\include" -isystem"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3" -isystem"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\cJSON-v1.7.15" -include"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

