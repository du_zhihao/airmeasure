################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../rt-thread/components/finsh/cmd.c \
../rt-thread/components/finsh/finsh_compiler.c \
../rt-thread/components/finsh/finsh_error.c \
../rt-thread/components/finsh/finsh_heap.c \
../rt-thread/components/finsh/finsh_init.c \
../rt-thread/components/finsh/finsh_node.c \
../rt-thread/components/finsh/finsh_ops.c \
../rt-thread/components/finsh/finsh_parser.c \
../rt-thread/components/finsh/finsh_token.c \
../rt-thread/components/finsh/finsh_var.c \
../rt-thread/components/finsh/finsh_vm.c \
../rt-thread/components/finsh/msh.c \
../rt-thread/components/finsh/shell.c 

OBJS += \
./rt-thread/components/finsh/cmd.o \
./rt-thread/components/finsh/finsh_compiler.o \
./rt-thread/components/finsh/finsh_error.o \
./rt-thread/components/finsh/finsh_heap.o \
./rt-thread/components/finsh/finsh_init.o \
./rt-thread/components/finsh/finsh_node.o \
./rt-thread/components/finsh/finsh_ops.o \
./rt-thread/components/finsh/finsh_parser.o \
./rt-thread/components/finsh/finsh_token.o \
./rt-thread/components/finsh/finsh_var.o \
./rt-thread/components/finsh/finsh_vm.o \
./rt-thread/components/finsh/msh.o \
./rt-thread/components/finsh/shell.o 

C_DEPS += \
./rt-thread/components/finsh/cmd.d \
./rt-thread/components/finsh/finsh_compiler.d \
./rt-thread/components/finsh/finsh_error.d \
./rt-thread/components/finsh/finsh_heap.d \
./rt-thread/components/finsh/finsh_init.d \
./rt-thread/components/finsh/finsh_node.d \
./rt-thread/components/finsh/finsh_ops.d \
./rt-thread/components/finsh/finsh_parser.d \
./rt-thread/components/finsh/finsh_token.d \
./rt-thread/components/finsh/finsh_var.d \
./rt-thread/components/finsh/finsh_vm.d \
./rt-thread/components/finsh/msh.d \
./rt-thread/components/finsh/shell.d 


# Each subdirectory must supply rules for building sources it contributes
rt-thread/components/finsh/%.o: ../rt-thread/components/finsh/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\include\libc" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\applications" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\board" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libcpu\cpu" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_drivers\config" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_drivers" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\ab32vg1_hal\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\ab32vg1_hal" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\bmsis\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\bmsis" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\bluetrum_sdk-latest" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\cJSON-v1.7.15" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\dht11-latest" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\drivers\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\drivers\sensors" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\finsh" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\libc\compilers\common" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\libc\compilers\newlib" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\include" -isystem"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3" -isystem"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\cJSON-v1.7.15" -include"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

