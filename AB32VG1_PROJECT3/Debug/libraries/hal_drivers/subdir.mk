################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libraries/hal_drivers/drv_common.c \
../libraries/hal_drivers/drv_gpio.c \
../libraries/hal_drivers/drv_rtc.c \
../libraries/hal_drivers/drv_usart.c \
../libraries/hal_drivers/drv_wdt.c 

OBJS += \
./libraries/hal_drivers/drv_common.o \
./libraries/hal_drivers/drv_gpio.o \
./libraries/hal_drivers/drv_rtc.o \
./libraries/hal_drivers/drv_usart.o \
./libraries/hal_drivers/drv_wdt.o 

C_DEPS += \
./libraries/hal_drivers/drv_common.d \
./libraries/hal_drivers/drv_gpio.d \
./libraries/hal_drivers/drv_rtc.d \
./libraries/hal_drivers/drv_usart.d \
./libraries/hal_drivers/drv_wdt.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/hal_drivers/%.o: ../libraries/hal_drivers/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\include\libc" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\applications" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\board" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libcpu\cpu" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_drivers\config" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_drivers" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\ab32vg1_hal\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\ab32vg1_hal" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\bmsis\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\libraries\hal_libraries\bmsis" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\bluetrum_sdk-latest" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\cJSON-v1.7.15" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\dht11-latest" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\drivers\include" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\drivers\sensors" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\finsh" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\libc\compilers\common" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\components\libc\compilers\newlib" -I"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rt-thread\include" -isystem"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3" -isystem"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\packages\cJSON-v1.7.15" -include"H:\RT_Thread_Studio\RT-ThreadStudio\workspace\AB32VG1_PROJECT3\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

