################################################################################
# 自动生成的文件。不要编辑！
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
OBJS := 
SECONDARY_FLASH := 
SECONDARY_LIST := 
SECONDARY_SIZE := 
ASM_DEPS := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
applications \
board \
libcpu/cpu \
libraries/hal_drivers \
libraries/hal_libraries/ab32vg1_hal/source \
libraries/hal_libraries/bmsis/source \
packages/cJSON-v1.7.15 \
packages/dht11-latest \
rt-thread/components/drivers/misc \
rt-thread/components/drivers/rtc \
rt-thread/components/drivers/sensors \
rt-thread/components/drivers/serial \
rt-thread/components/drivers/src \
rt-thread/components/drivers/watchdog \
rt-thread/components/finsh \
rt-thread/components/libc/compilers/common \
rt-thread/components/libc/compilers/newlib \
rt-thread/src \

