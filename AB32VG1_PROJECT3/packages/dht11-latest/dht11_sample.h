/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-11-28     admin       the first version
 */
#ifndef PACKAGES_DHT11_LATEST_DHT11_SAMPLE_H_
#define PACKAGES_DHT11_LATEST_DHT11_SAMPLE_H_

extern uint8_t temperature;
extern uint8_t humidity;

int dht11_read_temp_sample();

#endif /* PACKAGES_DHT11_LATEST_DHT11_SAMPLE_H_ */
